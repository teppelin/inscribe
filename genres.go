package main

import "encoding/json"

type genre struct {
	Genre       string
	Description string
}

func parseGenres(p []byte) []genre {
	res := []genre{}
	json.Unmarshal([]byte(p), &res)

	return res
}

// HandleGenres inserts/updates media genres
func HandleGenres(ctx JobContext) {
	genres := parseGenres(ctx.body)

	stmt := "insert into nekoani_public.genre (genre, genre_description) values ($1, $2) on conflict (genre) do nothing;"

	for _, genre := range genres {
		_, err := db.Exec(stmt, genre.Genre, genre.Description)

		CheckFailedInsert(err, "genre")
	}
}
