# inscribe

[![Build Status](https://drone.miraris.moe/api/badges/teppelin/inscribe/status.svg)](https://drone.miraris.moe/teppelin/inscribe)

amqp -> db sync service written in go.

## Building binary and Docker image

```sh
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o bin/inscribe
docker build -t registry.teppel.in/teppelin/inscribe .
```

To load the `.env` file:
```sh
export $(grep -v '^#' .env | xargs -0)
```

## Running

just set 2 env variables on the host

- `DATABASE_URL` - a user with insert grant on `nekoani_public` schema
- `AMQP_URI` - RabbitMQ URI
- `QUEUE_NAME` - Queue name from which the messages will be received

The worker listens on the `inscribe` queue on the default exchange.

### TODO:

- [ ] optimize languages/types and similar misc queries (load them into a map)
...

**P.S.** my first Golang app, I have no idea what I'm doing here, this service's architecture is most likely terrible and the program itself is shit.
I'll take all feedback and help I can get.

~~pls help~~
