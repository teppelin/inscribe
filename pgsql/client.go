package pgsqlclient

import (
	"database/sql"
	"log"
	"time"

	_ "github.com/lib/pq" // postgresql driver
)

// New creates a new pgsql connection
func New(connStr string) (db *sql.DB, err error) {
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	db.SetMaxOpenConns(25)
	db.SetMaxIdleConns(25)
	db.SetConnMaxLifetime(5 * time.Minute)

	if err = db.Ping(); err != nil {
		log.Fatal(err)
		return
	}

	return
}
