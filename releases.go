package main

import (
	"encoding/json"
	"log"
)

type chapter struct {
	AbsoluteNumber string
	Number         int
	Name           string
	Slug           string
	Type           string
	SourceURL      string
	Content        string
	Language       string
	Translator     string
}

type volume struct {
	Name     string
	Number   int
	Chapters []chapter
}

type response struct {
	MediaName    string
	ReleaseGroup string
	Volumes      []volume
}

func parseReleases(p []byte) response {
	res := response{}
	json.Unmarshal([]byte(p), &res)

	return res
}

// HandleReleases inserts/updates media releases
func HandleReleases(ctx JobContext) {
	res := parseReleases(ctx.body)

	var mediaID, releaseGroupID, volumeID int

	err := db.QueryRow("select media_id from nekoani_public.media_title where media_title ilike $1", res.MediaName).Scan(&mediaID)
	if err != nil {
		log.Printf("Couldn't find the media '%s'.", res.MediaName)
		return
	}

	err = db.QueryRow("select release_group_id from nekoani_public.release_group where release_group_name ilike $1", res.ReleaseGroup).Scan(&releaseGroupID)
	if err != nil {
		log.Printf("Couldn't find the release group '%s'.", res.ReleaseGroup)
		return
	}

	for _, vol := range res.Volumes {
		volumeID = SelectOrInsert(
			db.QueryRow("select volume_id from nekoani_public.volume where media_id = $1 and volume_number = $2", mediaID, vol.Number),
			`insert into nekoani_public.volume (media_id, volume_number, volume_name)
			 values ($1, $2, $3)
			 on conflict do nothing
			 returning volume_id`,
			mediaID, vol.Number, vol.Name,
		)

		// TODO:
		// this is litearlly garbage, maybe use sqlx to easily select types/langs into a struct
		for _, chpt := range vol.Chapters {
			var chapterTypeID, chapterID, languageID int

			chapterTypeID = SelectOrInsert(
				db.QueryRow("select chapter_type_id from nekoani_public.chapter_type where chapter_type = $1", chpt.Type),
				`insert into chapter_type (chapter_type) values ($1) returning chapter_type_id`,
				chpt.Type,
			)

			err = db.QueryRow("select language_id from nekoani_public.language where language_tag = $1", chpt.Language).Scan(&languageID)
			if err != nil {
				log.Printf("Language %s doesn't exist.", chpt.Language)
				continue
			}

			// TODO: do a single batch query for existing chapters, compare and insert (instead of select in a loop)
			chapterID = SelectOrInsert(
				db.QueryRow("select chapter_id from nekoani_public.chapter where volume_id = $1 and chapter_number = $2", volumeID, chpt.Number),
				`insert into chapter (chapter_type_id, volume_id, chapter_number, absolute_chapter_number, chapter)
			 values ($1, $2, $3, $4, $5)
			 on conflict do nothing
			 returning chapter_id`,
				chapterTypeID, volumeID, chpt.Number, chpt.AbsoluteNumber, chpt.Name,
			)

			_, err = db.Exec(
				`insert into nekoani_public.release (chapter_id, release_group_id, language_id, content, source_url, translator)
				values ($1, $2, $3, $4, $5, $6)
				on conflict on constraint release_chapter_id_release_group_id_language_id_key do update
				set content = $4`,
				chapterID,
				releaseGroupID,
				languageID,
				chpt.Content,
				chpt.SourceURL,
				chpt.Translator,
			)

			CheckFailedInsert(err, "release")
		}
	}
}
