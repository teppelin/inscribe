FROM golang:rc-alpine as builder
WORKDIR /build
COPY . .
RUN go build -o bin/inscribe

FROM alpine:edge

WORKDIR /usr/src/app
COPY --from=builder /build/bin/inscribe .

CMD ["./inscribe"]
