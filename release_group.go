package main

import (
	"encoding/json"
	"log"
)

type releaseGroupNames struct {
	Name string
	Type string
}

type releaseGroup struct {
	ID      string
	Names   []releaseGroupNames
	Contact ContactInfo
}

func parseReleaseGroup(g []byte) releaseGroup {
	res := releaseGroup{}
	json.Unmarshal([]byte(g), &res)

	return res
}

// HandleReleaseGroup inserts a release group
func HandleReleaseGroup(ctx JobContext) {
	releaseGroup := parseReleaseGroup(ctx.body)

	// TODO: update instead of simply returning
	if RowExists(
		"select 1 from nekoani_public.release_group_mapping where foreign_release_group_id = $1 and cataloging_app_id = $2",
		releaseGroup.ID,
		ctx.jobAppID,
	) {
		return
	}

	// local release group id
	var releaseGroupID int

	if len(releaseGroup.Names) == 0 {
		log.Println("releaseGroup: 'Names' is empty")
		return
	}

	stmt := `insert into nekoani_public.release_group (release_group_name, release_group_contact) values ($1, $2) returning release_group_id`
	err := db.QueryRow(
		stmt,
		releaseGroup.Names[0].Name,
		BuildContactRow(releaseGroup.Contact),
	).Scan(&releaseGroupID)

	if err != nil {
		log.Printf("Failed to insert nekoani_public.release_group: %s", err)
		return
	}

	stmt = `insert into nekoani_public.release_group_mapping values ($1, $2, $3)`
	_, err = db.Exec(stmt, releaseGroupID, ctx.jobAppID, releaseGroup.ID)
	CheckFailedInsert(err, "release_group_mapping")
}
