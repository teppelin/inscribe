package main

import (
	"encoding/json"
	"log"
)

type mediaTitles struct {
	Title string
	Type  string
}

type relatedMedia struct {
	Title string
	Type  string
}

type recommendedMedia struct {
	Title string
	Votes int
}

type ratings struct {
	One   int `json:"1"`
	Two   int `json:"2"`
	Three int `json:"3"`
	Four  int `json:"4"`
	Five  int `json:"5"`
}

type rating struct {
	average float64
	votes   float64
}

// Media media media
type media struct {
	ID                   int
	Cover                string
	Titles               []mediaTitles
	Category             string
	MediaState           string
	MediaType            string
	Synopsis             string
	RelatedMedia         []relatedMedia
	RecommendedMedia     []recommendedMedia
	Genres               []string
	Tags                 []string
	Ratings              ratings
	Language             string
	Writers              []string
	Illustrators         []string
	StartDate            string
	StatusInCOO          string
	Licensed             bool
	CompletelyTranslated bool
	MediaPublishers      []string
}

func parseMedia(a []byte) media {
	res := media{}
	err := json.Unmarshal([]byte(a), &res)
	if err != nil {
		log.Println("Failed to parse JSON: ", err)
	}

	return res
}

func calcRatings(x ratings) rating {
	votes := float64(x.One + x.Two + x.Three + x.Four + x.Five)

	if votes == 0 {
		return rating{0, 0}
	}

	return rating{
		AverageRating(x.One, x.Two, x.Three, x.Four, x.Five),
		float64(x.One + x.Two + x.Three + x.Four + x.Five),
	}
}

func insertMediaRelations(r []relatedMedia, mediaID int) {
	stmt := "insert into nekoani_public.media_relation (media1_id, media2_id, relation_id) values ($1, $2, $3) on conflict do nothing"

	for _, m := range r {
		var relationID, relatedMediaID int

		// find the relation
		err := db.QueryRow("select relation_id from nekoani_public.relation where relation = $1", m.Type).Scan(&relationID)
		if err != nil {
			db.QueryRow("insert into nekoani_public.relation (relation) values ($1) returning relation_id", m.Type).Scan(&relationID)
		}

		// find the related media
		err = db.QueryRow("select media_id from nekoani_public.media_title where media_title = $1 and media_name_type = 'main'", m.Title).Scan(&relatedMediaID)
		if err != nil {
			continue
		}

		_, err = db.Exec(stmt, mediaID, relatedMediaID, relationID)

		CheckFailedInsert(err, "media_relation")
	}
}

func insertMediaRecommendations(r []recommendedMedia, mediaID int) {
	stmt := "insert into nekoani_public.media_recommendation (media1_id, media2_id, approvals) values ($1, $2, $3) on conflict do nothing"

	for _, m := range r {
		var recommendedMediaID int

		// find the related media
		err := db.QueryRow("select media_id from nekoani_public.media_title where media_name_type = 'main'").Scan(&recommendedMediaID)
		if err != nil {
			continue
		}

		_, err = db.Exec(stmt, mediaID, recommendedMediaID, m.Votes)

		CheckFailedInsert(err, "nekoani_public.media_recommendation")
	}
}

func insertMediaAuthors(a []string, mediaID int, creditTypeID int) {
	lastInsertAuthorID := 0
	stmt := `insert into nekoani_public.media_author (author_id, media_id, credit_type_id)
	select author_id, $2, $3
	from nekoani_public.author_name
	where author_name ilike $1 on conflict do nothing returning author_id`

	for _, name := range a {
		err := db.QueryRow(stmt, name, mediaID, creditTypeID).Scan(&lastInsertAuthorID)
		// TODO: there should be a better way to handle this
		// it works incorrectly if a media really has multiple "main" authors
		if lastInsertAuthorID != 0 {
			break
		}
		CheckFailedInsert(err, "media_author")
	}
}

func insertMediaPublishers(p []string, mediaID int) {
	stmt := `insert into nekoani_public.media_publisher (publisher_id, media_id)
	select publisher_id, $2
	from nekoani_public.publisher_name
	where publisher_name = $1 on conflict do nothing`

	for _, name := range p {
		_, err := db.Exec(stmt, name, mediaID)

		CheckFailedInsert(err, "media_publisher")
	}
}

func insertMediaPoster(p string, mediaID int) {
	stmt := `insert into nekoani_public.poster (media_id, main, poster_url)
	values ($1, true, $2) on conflict do nothing`

	_, err := db.Exec(stmt, mediaID, p)
	CheckFailedInsert(err, "poster")
}

// HandleMedia whatever
func HandleMedia(ctx JobContext) {
	media := parseMedia(ctx.body)
	writerCreditID := 959
	illustratorCreditID := 825

	var mediaID, mediaStateID int

	err := db.QueryRow("select media_state_id from nekoani_public.media_state where media_state = $1", media.MediaState).Scan(&mediaStateID)

	if err != nil {
		db.QueryRow("insert into nekoani_public.media_state (media_state) values ($1) returning media_state_id", media.MediaState).Scan(&mediaStateID)
	}

	// TODO: merge novels with varying cataloging_app_id
	err = db.QueryRow("select media_id from nekoani_public.media_mapping where foreign_media_id = $1 and cataloging_app_id = $2", media.ID, ctx.jobAppID).Scan(&mediaID)

	// means we have an ID
	if err == nil {
		/**
		Weighted Rank (WR) = (v / (v + m)) * S + (m / (v + m)) * C
			S = Average score for the Media (mean).
			v = Number of votes for the Media = (Number of people scoring the Media).
			m = Minimum votes/scores required to get a calculated score (currently 50 scores required).
			C = The mean score across the entire DB.
		*/
		var C, v, S, WR float64
		var m float64 = 50

		err := db.QueryRow("select avg(average_rating) as C from nekoani_public.media where average_rating != 'NaN'").Scan(&C)

		if err != nil {
			C = 3.0 // KEKW
		}

		rs := calcRatings(media.Ratings)
		S = rs.average
		v = rs.votes

		// avoid NaN values
		if v != 0 {
			WR = (v/(v+m))*S + (m/(v+m))*C
		}

		stmt := `update nekoani_public.media set
		media_state_id = $2,
		average_rating = $3,
		weighted_rating = coalesce($4, 0.0),
		media_votes = $5,
		status_in_coo = $6,
		completely_translated = $7,
		start_date = to_date($8, 'yyyy')
		where media_id = $1`

		_, err = db.Exec(stmt, mediaID, mediaStateID, S, WR, v, media.StatusInCOO, media.CompletelyTranslated, NewNullString(media.StartDate))
		CheckFailedInsert(err, "media")

		insertMediaAuthors(media.Writers, mediaID, writerCreditID)
		insertMediaAuthors(media.Illustrators, mediaID, illustratorCreditID)
		insertMediaPublishers(media.MediaPublishers, mediaID)
		insertMediaRelations(media.RelatedMedia, mediaID)
		insertMediaRecommendations(media.RecommendedMedia, mediaID)

		return
	}

	// local media id
	var categoryID, mediaTypeID, languageID int

	// non nullable fields
	err = db.QueryRow("select category_id from nekoani_public.category where category = $1", media.Category).Scan(&categoryID)
	if err != nil {
		log.Println("category: ", media.Category, err)
		return
	}
	err = db.QueryRow("select language_id from nekoani_public.language where alpha2 = $1 or language_tag = $1", media.Language).Scan(&languageID)
	if err != nil {
		log.Println("language: ", media.Language, err)
		return
	}
	err = db.QueryRow("select media_type_id from nekoani_public.media_type where media_type = $1", media.MediaType).Scan(&mediaTypeID)
	if err != nil {
		db.QueryRow("insert into media_type (media_type) values ($1) returning media_type_id", media.MediaType).Scan(&mediaTypeID)
	}

	rs := media.Ratings

	stmt := `insert into nekoani_public.media (
		category_id,
		media_state_id,
		media_type_id,
		language_id,
		status_in_coo,
		synopsis,
		start_date,
		average_rating,
		media_votes,
		licensed,
		completely_translated,
		nsfw
	) values ($1, $2, $3, $4, $5, $6, to_date($7, 'yyyy'), $8, $9, $10, $11, $12)
	returning media_id`
	err = db.QueryRow(
		stmt,
		categoryID,
		mediaStateID,
		mediaTypeID,
		languageID,
		media.StatusInCOO,
		NewNullString(media.Synopsis),
		NewNullString(media.StartDate),
		AverageRating(rs.One, rs.Two, rs.Three, rs.Four, rs.Five),
		rs.One+rs.Two+rs.Three+rs.Four+rs.Five,
		media.Licensed,
		media.CompletelyTranslated,
		false,
	).Scan(&mediaID)

	if err != nil {
		log.Println("Failed to insert media: ", err)
		return
	}

	// TODO: use bulk insert statements, also DRY

	stmt = "insert into nekoani_public.media_title (media_id, media_title, media_name_type) values ($1, $2, $3)"

	for _, title := range media.Titles {
		_, err = db.Exec(stmt, mediaID, title.Title, title.Type)

		CheckFailedInsert(err, "media_title")
	}

	insertMediaAuthors(media.Writers, mediaID, writerCreditID)
	insertMediaAuthors(media.Illustrators, mediaID, illustratorCreditID)
	insertMediaPublishers(media.MediaPublishers, mediaID)

	stmt = `insert into nekoani_public.media_genre (genre_id, media_id)
	select genre_id, $2
	from nekoani_public.genre
	where genre = $1`

	for _, name := range media.Genres {
		_, err = db.Exec(stmt, name, mediaID)

		CheckFailedInsert(err, "media_genre")
	}

	stmt = `insert into nekoani_public.media_tag (tag_id, media_id)
	select tag_id, $2
	from nekoani_public.tag
	where tag = $1`

	for _, name := range media.Tags {
		_, err = db.Exec(stmt, name, mediaID)

		CheckFailedInsert(err, "media_tag")
	}

	stmt = `insert into nekoani_public.media_mapping values ($1, $2, $3)`
	_, err = db.Exec(stmt, mediaID, ctx.jobAppID, media.ID)

	CheckFailedInsert(err, "media_mapping")

	// insert the poster
	if len(media.Cover) <= 0 {
		return
	}
	// u, _ := url.Parse(media.Cover)
	// r := <-UploadResource("poster", mediaID, u)
	// if len(r) <= 0 {
	// 	return
	// }
	insertMediaPoster(media.Cover, mediaID)
}
