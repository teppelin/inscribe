module code.miraris.moe/teppelin/inscribe

go 1.12

require (
	github.com/gammazero/workerpool v0.0.0-20200608033439-1a5ca90a5753
	github.com/google/uuid v1.1.1
	github.com/lib/pq v1.7.0
	github.com/minio/minio-go/v6 v6.0.57
	github.com/streadway/amqp v1.0.0
)
