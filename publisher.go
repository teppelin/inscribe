package main

import (
	"encoding/json"
	"log"
)

// Publisher a media publisher

type publisherName struct {
	Name     string
	Type     string
	Language string
}

type publisher struct {
	ID       string
	Names    []publisherName
	Note     string
	Contact  ContactInfo
	Language string
}

func parsePublisher(p []byte) publisher {
	res := publisher{}
	json.Unmarshal([]byte(p), &res)

	return res
}

// HandlePublisher inserts/updates a publisher
func HandlePublisher(ctx JobContext) {
	publisher := parsePublisher(ctx.body)

	// TODO: update instead of simply returning
	if RowExists(
		"select 1 from nekoani_public.publisher_mapping where foreign_publisher_id = $1 and cataloging_app_id = $2",
		publisher.ID,
		ctx.jobAppID,
	) {
		return
	}

	// local publisher id
	var publisherID int

	stmt := `insert into nekoani_public.publisher (language_id, publisher_contact, publisher_note)
	select language_id, $2, $3
	from nekoani_public.language
	where language_tag = $1
	returning publisher_id`
	err := db.QueryRow(
		stmt,
		publisher.Language,
		BuildContactRow(publisher.Contact),
		publisher.Note,
	).Scan(&publisherID)

	if err != nil {
		log.Printf("Failed to insert publisher: %s", err)
		return
	}

	stmt = `insert into nekoani_public.publisher_name (publisher_id, publisher_name, publisher_name_type) values ($1, $2, $3)`

	for _, name := range publisher.Names {
		_, err = db.Exec(stmt, publisherID, name.Name, name.Type)

		CheckFailedInsert(err, "publisher_name")
	}

	stmt = `insert into nekoani_public.publisher_mapping values ($1, $2, $3)`
	_, err = db.Exec(stmt, publisherID, ctx.jobAppID, publisher.ID)

	CheckFailedInsert(err, "publisher_mapping")
}
