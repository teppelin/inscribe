package main

import (
	"fmt"
	"log"
	"mime"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/google/uuid"
	"github.com/minio/minio-go/v6"
)

var torProxyURL = os.Getenv("PROXY_URL")

func shouldProxy(hn string) bool {
	switch hn {
	case
		"mangaupdates.com",
		"www.mangaupdates.com",
		"example.com":
		return true
	}
	return false
}

// UploadResource send an object to our bucket
func UploadResource(resource string, id int, objURL *url.URL) <-chan string {
	r := make(chan string)

	go func() {
		defer close(r)

		client := &http.Client{}
		if shouldProxy(objURL.Hostname()) {
			u, _ := url.Parse(torProxyURL)
			client.Transport = &http.Transport{Proxy: http.ProxyURL(u)}
		}

		file, err := client.Get(objURL.String())
		if err != nil {
			log.Println(err)
			return
		}
		ext := file.Header.Get("content-type")
		if ext == "" {
			return
		}
		m, err := mime.ExtensionsByType(ext)
		if err != nil || len(m) == 0 {
			return
		}

		// create a bucket
		exists, errBucketExists := minioClient.BucketExists(resource)
		if !exists {
			if errBucketExists != nil {
				log.Println(errBucketExists)
				return
			}
			// TODO: set read-only policy
			err := minioClient.MakeBucket(resource, "eu-west-1")
			if err != nil {
				log.Println(err)
				return
			}
		}

		var objName strings.Builder
		objName.WriteString(strconv.Itoa(id))
		objName.WriteString("/")
		objName.WriteString(uuid.New().String())
		objName.WriteString(m[0])
		_, err = minioClient.PutObject(
			resource,
			objName.String(),
			file.Body,
			-1,
			minio.PutObjectOptions{},
		)
		if err != nil {
			fmt.Println(err)
			return
		}

		link := minioClient.EndpointURL()
		link.Path = resource + "/" + objName.String()

		r <- link.String()
	}()

	return r
}
