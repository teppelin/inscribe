package main

import (
	"database/sql"
	"fmt"
	"log"
	"math"
)

// NewNullString creates a string or null
func NewNullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}

// RowExists checks whether a row exists
func RowExists(query string, args ...interface{}) bool {
	var exists bool
	query = fmt.Sprintf("select exists (%s)", query)
	err := db.QueryRow(query, args...).Scan(&exists)

	if err != nil && err != sql.ErrNoRows {
		log.Fatalf("error checking if A row exists '%s' %v", args, err)
	}
	return exists
}

// BuildContactRow builds a contact_info composite type row
func BuildContactRow(contact ContactInfo) string {
	return fmt.Sprintf(
		"(%s,%s,%s,%s,%s,%s,%s)",
		contact.OfficialWebsite,
		contact.Twitter,
		contact.Facebook,
		contact.IRC,
		contact.Forum,
		contact.Discord,
		contact.Mail,
	)
}

// CheckFatal checks for a fatal error and exists if one exists
func CheckFatal(err error) {
	if err != nil {
		log.Fatal("ERROR: ", err)
	}
}

// CheckFailedInsert checks for a non-fatal insert error and logs it
func CheckFailedInsert(err error, entity string) {
	if err != nil {
		log.Println(entity, err)
	}
}

// SelectOrInsert selects and returns PK if it exists, otherwise selects and returns
func SelectOrInsert(sel *sql.Row, insertQuery string, args ...interface{}) int {
	var id int
	err := sel.Scan(&id)

	if err != nil {
		db.QueryRow(insertQuery, args...).Scan(&id)
	}

	return id
}

// RoundDoublePrec rounds a float to double precision
func RoundDoublePrec(x float64) float64 {
	return math.Round(x*100) / 100
}

// AverageRating 5 ints / 5
func AverageRating(a int, b int, c int, d int, e int) float64 {
	return float64(1*a+2*b+3*c+4*d+5*e) / float64(a+b+c+d+e)
}
