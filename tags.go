package main

import "encoding/json"

type tag struct {
	Tag         string
	Description string
}

func parseTags(p []byte) []tag {
	res := []tag{}
	json.Unmarshal([]byte(p), &res)

	return res
}

// HandleTags inserts/updates media tags
func HandleTags(ctx JobContext) {
	tags := parseTags(ctx.body)

	stmt := "insert into nekoani_public.tag (tag, tag_description) values ($1, $2) on conflict (tag) do nothing;"

	for _, tag := range tags {
		_, err := db.Exec(stmt, tag.Tag, tag.Description)

		CheckFailedInsert(err, "tag")
	}
}
