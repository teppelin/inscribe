package main

// ContactInfo composite type
type ContactInfo struct {
	OfficialWebsite string
	Twitter         string
	Facebook        string
	IRC             string
	Forum           string
	Discord         string
	Mail            string
}
