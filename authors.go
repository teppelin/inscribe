package main

import (
	"encoding/json"
	"log"
	"net/url"
)

type authorNames struct {
	Name string
	Type string
}

// Author media author
type author struct {
	ID         string
	Names      []authorNames
	Photo      string
	BirthPlace string
	BirthDate  string
	Comment    string
	Gender     string
	Contact    ContactInfo
}

func parseAuthor(a []byte) author {
	res := author{}
	json.Unmarshal([]byte(a), &res)

	return res
}

// HandleAuthor whatever
func HandleAuthor(ctx JobContext) {
	author := parseAuthor(ctx.body)

	var genderID int

	// ISO/IEC 5218
	if author.Gender == "Male" {
		genderID = 1
	} else if author.Gender == "Female" {
		genderID = 2
	} else {
		genderID = 0
	}

	// TODO: update instead of simply returning
	if RowExists(
		"select 1 from nekoani_public.author_mapping where foreign_author_id = $1 and cataloging_app_id = $2",
		author.ID,
		ctx.jobAppID,
	) {
		return
	}

	// local author id
	var authorID int

	stmt := `insert into nekoani_public.author (gender_id, author_contact, author_note, author_photo) values ($1, $2, $3, $4) returning author_id`
	err := db.QueryRow(
		stmt,
		genderID,
		BuildContactRow(author.Contact),
		author.Comment,
		nil,
	).Scan(&authorID)

	if err != nil {
		log.Printf("Failed to insert author: %s", err)
		return
	}

	stmt = `insert into nekoani_public.author_name (author_id, author_name, author_name_type) values ($1, $2, $3)`

	for _, name := range author.Names {
		_, err = db.Exec(stmt, authorID, name.Name, name.Type)

		CheckFailedInsert(err, "author_name")
	}

	stmt = `insert into nekoani_public.author_mapping values ($1, $2, $3)`
	_, err = db.Exec(stmt, authorID, ctx.jobAppID, author.ID)

	CheckFailedInsert(err, "author_mapping")

	// upload image
	// stmt = `insert into `
	if len(author.Photo) > 0 {
		u, _ := url.Parse(author.Photo)
		// r := <-UploadResource("author", authorID, u)
		// if len(r) == 0 {
		// 	return
		// }
		// TODO
		stmt = `update nekoani_public.author set author_photo = $1 where author_id = $2`
		_, err = db.Exec(stmt, u.String(), authorID)
	}
}
