package main

import (
	"database/sql"
	"log"
	"os"
	"strconv"
	"time"

	amqpclient "code.miraris.moe/teppelin/inscribe/amqp"
	"code.miraris.moe/teppelin/inscribe/backoff"
	pgsqlclient "code.miraris.moe/teppelin/inscribe/pgsql"
	"github.com/gammazero/workerpool"
	"github.com/minio/minio-go/v6"
	"github.com/streadway/amqp"
)

// JobContext app's job/delivery context type
type JobContext struct {
	body     []byte
	jobType  string
	jobAppID int
}

// global db conn
var db *sql.DB

// minio client
var minioClient *minio.Client

func init() {
	var err error
	db, err = pgsqlclient.New(os.Getenv("DATABASE_URL"))
	ssl, err := strconv.ParseBool(os.Getenv("MINIO_SSL"))
	minioClient, err = minio.New(
		os.Getenv("MINIO_ENDPOINT"),
		os.Getenv("MINIO_ACCESS_KEY"),
		os.Getenv("MINIO_SECRET"),
		ssl,
	)

	CheckFatal(err)
}

func main() {
	var err error
	concurrencyLimit := 25
	if env := os.Getenv("CONCURRENCY_LIMIT"); env != "" {
		concurrencyLimit, err = strconv.Atoi(env)
		CheckFatal(err)
	}
	queue := amqpclient.New(os.Getenv("QUEUE_NAME"), os.Getenv("AMQP_URI"))

	// reconnect
	b := &backoff.Backoff{}
	var msgs <-chan amqp.Delivery
	for {
		msgs, err = queue.Stream()
		if err == nil {
			break
		}
		d := b.Duration()
		log.Printf("%s, retrying in %s", err, d)
		time.Sleep(d)
		continue
	}

	// consume
	forever := make(chan bool)
	go handle(msgs, concurrencyLimit)
	log.Printf("✔️ Connected and listening for jobs.")
	<-forever
}

func handle(deliveries <-chan amqp.Delivery, l int) {
	wp := workerpool.New(l)
	log.Printf("Created a pool of %d workers", l)

	for d := range deliveries {
		log.Printf("Received job of type %s from appId %s", d.Type, d.AppId)
		appID, _ := strconv.Atoi(d.AppId)
		ctx := JobContext{d.Body, d.Type, appID}

		wp.Submit(func() {
			switch d.Type {
			case "author":
				HandleAuthor(ctx)
			case "releaseGroup":
				HandleReleaseGroup(ctx)
			case "publisher":
				HandlePublisher(ctx)
			case "genres":
				HandleGenres(ctx)
			case "tags":
				HandleTags(ctx)
			case "media":
				HandleMedia(ctx)
			case "releases":
				HandleReleases(ctx)
			default:
				log.Println("Unknown response received:", string(d.Body))
			}
		})
		// the ack doesn't imply the data has been successfully processed,
		// just that we've passed it to our workers
		d.Ack(false)
	}
}
